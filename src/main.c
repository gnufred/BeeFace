#include <pebble.h>

static Window *s_main_window;

TextLayer *s_months_layer[13];
TextLayer *s_weekdays_layer[8];

static TextLayer *s_date_layer;
static TextLayer *s_time_layer;
static TextLayer *s_time2_layer;
static TextLayer *s_battery_layer;
static TextLayer *s_bluetooth_layer;
static TextLayer *s_sleep_bar_layer;
static TextLayer *s_deep_sleep_bar_layer;
static TextLayer *s_step_layer;
static TextLayer *s_sleep_meter;

static void create_indicator_layer(
        Layer *window_layer,
        TextLayer *indicator_layer,
        const char *label
        ) {
    text_layer_set_background_color(indicator_layer, GColorBulgarianRose);
    text_layer_set_text_color(indicator_layer, GColorWhite);
    text_layer_set_font(indicator_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
    text_layer_set_text_alignment(indicator_layer, GTextAlignmentCenter);
    text_layer_set_text(indicator_layer, label);
    layer_add_child(window_layer, text_layer_get_layer(indicator_layer));
}

static void create_months_layer(Layer *window_layer) {

    const char * month_first_letter[] = {
        "J","F","M","A","M","J","J","A","S","O","N","D"
    };

    for(int i=1; i <= 12; i++) {
        s_months_layer[i] = text_layer_create(
                GRect(0+((i-1)*12), 0, 12, 24)
                );
        create_indicator_layer(
                window_layer,
                s_months_layer[i],
                month_first_letter[i-1]
                );
    }
}

static void create_weekdays_layer(Layer *window_layer) {
    const char * weekday_first_letter[] = {
        "L","M","M","J","V","S","D"
    };

    for(int i=1; i <= 7; i++) {
        s_weekdays_layer[i] = text_layer_create(
                GRect(0+((i-1)*12), 144, 12, 24)
                );
        create_indicator_layer(
                window_layer,
                s_weekdays_layer[i],
                weekday_first_letter[i-1]
                );
    }
}

static void create_date_layer(Layer *window_layer) {
    s_date_layer = text_layer_create(GRect(0, 30, 144, 26));
    text_layer_set_background_color(s_date_layer, GColorDarkCandyAppleRed);
    text_layer_set_text_color(s_date_layer, GColorWhite);
    text_layer_set_font(s_date_layer, fonts_get_system_font(FONT_KEY_LECO_20_BOLD_NUMBERS));
    text_layer_set_text_alignment(s_date_layer, GTextAlignmentCenter);
    layer_add_child(window_layer, text_layer_get_layer(s_date_layer));
}

static void create_time_layer(Layer *window_layer) {
    s_time_layer = text_layer_create(GRect(0, 56, 144, 56));
    text_layer_set_background_color(s_time_layer, GColorYellow);
    text_layer_set_text_color(s_time_layer, GColorBlack);
    text_layer_set_font(s_time_layer, fonts_get_system_font(FONT_KEY_LECO_42_NUMBERS));
    text_layer_set_text_alignment(s_time_layer, GTextAlignmentCenter);
    layer_add_child(window_layer, text_layer_get_layer(s_time_layer));
}

static void create_second_time_layer(Layer *window_layer) {
    s_time2_layer = text_layer_create(GRect(0, 112, 86, 26));
    text_layer_set_background_color(s_time2_layer, GColorDarkCandyAppleRed);
    text_layer_set_text_color(s_time2_layer, GColorWhite);
    text_layer_set_font(s_time2_layer, fonts_get_system_font(FONT_KEY_LECO_20_BOLD_NUMBERS));
    text_layer_set_text_alignment(s_time2_layer, GTextAlignmentCenter);
    layer_add_child(window_layer, text_layer_get_layer(s_time2_layer));
}

static void create_battery_layer(Layer *window_layer) {
    s_battery_layer = text_layer_create(GRect(86, 112, 58, 26));
    text_layer_set_background_color(s_battery_layer, GColorDarkCandyAppleRed);
    text_layer_set_text_color(s_battery_layer, GColorWhite);
    text_layer_set_font(s_battery_layer, fonts_get_system_font(FONT_KEY_LECO_20_BOLD_NUMBERS));
    text_layer_set_text_alignment(s_battery_layer, GTextAlignmentCenter);
    layer_add_child(window_layer, text_layer_get_layer(s_battery_layer));
}

static void create_step_count_layer(Layer *window_layer) {
    s_step_layer = text_layer_create(GRect(84, 144, 60, 24));
    text_layer_set_background_color(s_step_layer, GColorBulgarianRose);
    text_layer_set_text_color(s_step_layer, GColorWhite);
    text_layer_set_font(s_step_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
    text_layer_set_text_alignment(s_step_layer, GTextAlignmentCenter);
    layer_add_child(window_layer, text_layer_get_layer(s_step_layer));
}

static void create_bluetooth_layer(Layer *window_layer) {
    s_bluetooth_layer = text_layer_create(GRect(0, 24, 144, 6));
    text_layer_set_background_color(s_bluetooth_layer, GColorWhite);
    layer_add_child(window_layer, text_layer_get_layer(s_bluetooth_layer));
    layer_set_hidden(text_layer_get_layer(s_bluetooth_layer), true);
}

static void create_sleep_bar_layer(Layer *window_layer) {
    s_sleep_bar_layer = text_layer_create(GRect(0, 138, 0, 6));
    text_layer_set_background_color(s_sleep_bar_layer, GColorBlueMoon);
    layer_add_child(window_layer, text_layer_get_layer(s_sleep_bar_layer));
}

static void create_deep_sleep_bar_layer(Layer *window_layer) {
    s_deep_sleep_bar_layer = text_layer_create(GRect(0, 138, 0, 6));
    text_layer_set_background_color(s_deep_sleep_bar_layer, GColorOxfordBlue);
    layer_add_child(window_layer, text_layer_get_layer(s_deep_sleep_bar_layer));
}

static void create_sleep_meter_layer(Layer *window_layer) {

    for(int i=9; i <= 144; i=i+9) {

        if(i % 18 == 0) {
            s_sleep_meter = text_layer_create(GRect(i, 138, 1, 6));
        } else {
            s_sleep_meter = text_layer_create(GRect(i, 138, 1, 3));
        }

        text_layer_set_background_color(s_sleep_meter, GColorWhite);
        layer_add_child(window_layer, text_layer_get_layer(s_sleep_meter));

    }
}

// W: 144px H: 168px
static void main_window_load(Window *window) {

    Layer *window_layer = window_get_root_layer(window);
    window_set_background_color(s_main_window, GColorBlack);

    create_months_layer(window_layer);
    create_weekdays_layer(window_layer);
    create_date_layer(window_layer);
    create_time_layer(window_layer);
    create_second_time_layer(window_layer);
    create_battery_layer(window_layer);
    create_step_count_layer(window_layer);
    create_bluetooth_layer(window_layer);
    create_sleep_bar_layer(window_layer);
    create_deep_sleep_bar_layer(window_layer);
    create_sleep_meter_layer(window_layer);
}

static void main_window_unload(Window *window) {
    for(int i=1; i <= 7; i++) text_layer_destroy(s_weekdays_layer[i]);
    for(int i=1; i <= 12; i++) text_layer_destroy(s_months_layer[i]);
    text_layer_destroy(s_date_layer);
    text_layer_destroy(s_time_layer);
    text_layer_destroy(s_time2_layer);
    text_layer_destroy(s_battery_layer);
    text_layer_destroy(s_bluetooth_layer);
    text_layer_destroy(s_sleep_bar_layer);
    text_layer_destroy(s_deep_sleep_bar_layer);
    text_layer_destroy(s_step_layer);
}



static void bluetooth_callback(bool connected) {
    if(!connected) {
        layer_set_hidden(text_layer_get_layer(s_bluetooth_layer), false);
    } else {
        layer_set_hidden(text_layer_get_layer(s_bluetooth_layer), true);
    }
}



static void battery_callback(BatteryChargeState state) {
    static char s_battery_buffer[32];

    if (state.is_charging) {
        snprintf(s_battery_buffer, sizeof(s_battery_buffer), "%s", "--");
        text_layer_set_text(s_battery_layer, s_battery_buffer);
    } else {
        snprintf(s_battery_buffer, sizeof(s_battery_buffer), "%d", state.charge_percent);
        text_layer_set_text(s_battery_layer, s_battery_buffer);
    }
}



void update_indicator_layer(TextLayer *old_layer, TextLayer *new_layer) {
    text_layer_set_background_color(old_layer, GColorBulgarianRose);
    text_layer_set_text_color(old_layer, GColorWhite);
    text_layer_set_background_color(new_layer, GColorYellow);
    text_layer_set_text_color(new_layer, GColorBlack);
}

void update_weekdays_layer(int tm_wday) {
    switch ( tm_wday + 1 ) {
        // 1 is Sunday
        case 1: update_indicator_layer(s_weekdays_layer[6], s_weekdays_layer[7]); break;
        case 2: update_indicator_layer(s_weekdays_layer[7], s_weekdays_layer[1]); break;
        case 3: update_indicator_layer(s_weekdays_layer[1], s_weekdays_layer[2]); break;
        case 4: update_indicator_layer(s_weekdays_layer[2], s_weekdays_layer[3]); break;
        case 5: update_indicator_layer(s_weekdays_layer[3], s_weekdays_layer[4]); break;
        case 6: update_indicator_layer(s_weekdays_layer[4], s_weekdays_layer[5]); break;
        case 7: update_indicator_layer(s_weekdays_layer[5], s_weekdays_layer[6]); break;
    }
}

void update_months_layer(int tm_mon) {
    switch ( tm_mon + 1 ) {
        case 1 : update_indicator_layer(s_months_layer[12], s_months_layer[1 ]); break;
        case 2 : update_indicator_layer(s_months_layer[1 ], s_months_layer[2 ]); break;
        case 3 : update_indicator_layer(s_months_layer[2 ], s_months_layer[3 ]); break;
        case 4 : update_indicator_layer(s_months_layer[3 ], s_months_layer[4 ]); break;
        case 5 : update_indicator_layer(s_months_layer[4 ], s_months_layer[5 ]); break;
        case 6 : update_indicator_layer(s_months_layer[5 ], s_months_layer[6 ]); break;
        case 7 : update_indicator_layer(s_months_layer[6 ], s_months_layer[7 ]); break;
        case 8 : update_indicator_layer(s_months_layer[7 ], s_months_layer[8 ]); break;
        case 9 : update_indicator_layer(s_months_layer[8 ], s_months_layer[9 ]); break;
        case 10: update_indicator_layer(s_months_layer[9 ], s_months_layer[10]); break;
        case 11: update_indicator_layer(s_months_layer[10], s_months_layer[11]); break;
        case 12: update_indicator_layer(s_months_layer[11], s_months_layer[12]); break;
    }
}

static void update_date_buffer(struct tm *tick_time) {
    static char s_date_buffer[16];
    strftime(s_date_buffer, sizeof(s_date_buffer), "%F", tick_time);
    text_layer_set_text(s_date_layer, s_date_buffer);
}

static void update_time_buffer(struct tm *tick_time) {
    static char s_time_buffer[8];
    strftime(
            s_time_buffer,
            sizeof(s_time_buffer),
            clock_is_24h_style() ? "%H:%M" : "%I:%M",
            tick_time
            );
    text_layer_set_text(s_time_layer, s_time_buffer);
}

static void update_second_time_buffer(struct tm *gm_time) {
    static char s_time2_buffer[8];
    strftime(
            s_time2_buffer,
            sizeof(s_time2_buffer),
            clock_is_24h_style() ? "%H:%M" : "%I:%M",
            gm_time
            );
    text_layer_set_text(s_time2_layer, s_time2_buffer);
}

static void update_step_count_buffer(){
    HealthMetric metric = HealthMetricStepCount;
    static char s_step_buffer[10];
    snprintf(
            s_step_buffer,
            sizeof(s_step_buffer), "%d", (int)health_service_sum_today(metric)
            );
    text_layer_set_text(s_step_layer, s_step_buffer);
}

static void update_sleep_bar_buffer() {
    HealthMetric sleep_metric = HealthMetricSleepSeconds;
    HealthMetric deep_sleep_metric = HealthMetricSleepRestfulSeconds;

    static double seconds_in_8_hours = 28000;
    static double pebble_time_pixel_width = 144;

    double s_sleep_pixel_length = (double)health_service_sum_today(sleep_metric) / seconds_in_8_hours * pebble_time_pixel_width;
    double s_deep_sleep_pixel_length = (double)health_service_sum_today(deep_sleep_metric) / seconds_in_8_hours * pebble_time_pixel_width;

    text_layer_set_size(s_sleep_bar_layer, GSize(s_sleep_pixel_length, 6));
    text_layer_set_size(s_deep_sleep_bar_layer, GSize(s_deep_sleep_pixel_length, 6));
}

static void show_data() {
    time_t temp = time(NULL);
    struct tm *tick_time = localtime(&temp);
    struct tm *gm_time = gmtime(&temp);

    update_months_layer(tick_time->tm_mon);
    update_weekdays_layer(tick_time->tm_wday);
    update_date_buffer(tick_time);
    update_step_count_buffer();
    update_sleep_bar_buffer();
    update_time_buffer(tick_time);
    update_second_time_buffer(gm_time);
}


static void tick_handler(struct tm *tick_time, TimeUnits units_changed) {

    if ( units_changed >= MONTH_UNIT ) {
        update_months_layer(tick_time->tm_mon);
    }

    if ( units_changed >= DAY_UNIT ) {
        update_weekdays_layer(tick_time->tm_wday);
        update_date_buffer(tick_time);
    }

    if ( units_changed >= MINUTE_UNIT ) {
        time_t temp = time(NULL);
        struct tm *gm_time = gmtime(&temp);
        update_time_buffer(tick_time);
        update_second_time_buffer(gm_time);
        if(tick_time->tm_min % 5 == 0) {
            update_step_count_buffer();
            update_sleep_bar_buffer();
        }
    }

}



static void init() {
    // Create main Window element and assign to pointer
    s_main_window = window_create();

    // Set handlers to manage the elements inside the Window
    window_set_window_handlers(s_main_window, (WindowHandlers) {
            .load = main_window_load,
            .unload = main_window_unload
            });

    // Show the Window on the watch, with animated=true
    window_stack_push(s_main_window, true);

    // Make sure the time is displayed from the start
    show_data();

    // Make sure the battery level is displayed from the start
    battery_callback(battery_state_service_peek());

    // Make sure bluetooth connection status is displayed from the start
    bluetooth_callback(connection_service_peek_pebble_app_connection());

    // Register with TickTimerService
    tick_timer_service_subscribe(MINUTE_UNIT, tick_handler);

    // Registering battery status services
    battery_state_service_subscribe(battery_callback);

    // Registering bluetooth status services
    connection_service_subscribe((ConnectionHandlers) {
            .pebble_app_connection_handler = bluetooth_callback
            });
}

static void deinit() {
    window_destroy(s_main_window);
}



int main(void) {
    init();
    app_event_loop();
    deinit();
}
